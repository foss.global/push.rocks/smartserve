import * as plugins from './smartserve.plugins.js';

export const packageDir = plugins.path.join(
  plugins.smartpath.get.dirnameFromImportMetaUrl(import.meta.url),
  '../'
);

export const bundlePath = plugins.path.join(packageDir, './dist_ts_web/bundle.js');
